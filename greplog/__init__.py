#!/usr/bin/env python

"""
greplog
~~~~~~~

Usage::

    >>> 

:copyright: (c) 2014 by Johan Nestaas.
:license: GPLv3, see LICENSE for more details.

"""

__title__ = 'greplog'
__version__ = '0.0.1'
__author__ = 'Johan Nestaas'
__license__ = 'GPLv3'
__copyright__ = 'Copyright 2014 Johan Nestaas'


